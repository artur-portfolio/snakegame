const canvas = document.getElementById('game')
const ctx = canvas.getContext('2d')

const ground = new Image()
ground.src = 'img/canvas_bg.png'

const foodImg = new Image()
foodImg.src = 'img/food_icon.png'
foodImg.height = '32px'
foodImg.width = '32px'

let box = 32
let score = 0

let food = {
  x: Math.floor(Math.random() * 18) * box,
  y: Math.floor(Math.random() * 18) * box,
}

let snake = []
snake[0] = {
  x: 9 * box,
  y: 10 * box
}

document.addEventListener('keydown', direction);

let dir;

function direction(e){
  if(e.keyCode === 39 && dir != 'right'){
    dir = 'left'
  }else if(e.keyCode === 38 && dir !='bottom'){
    dir = 'top'
  }else if(e.keyCode === 37 && dir !='left'){
    dir = 'right'
  }else if(e.keyCode === 40 && dir !='top'){
    dir = 'bottom'

  }
}

function eatTail(head, arr){
  for(let i in arr){
    if(head.x === arr[i].x && head.y === arr[i].y){
      clearInterval(game)
    }
  }
}

function drawGame(){
  ctx.drawImage(ground, 0, 0)

  ctx.drawImage(foodImg, food.x, food.y)

  for(let i in snake){
    ctx.fillStyle = i == 0 ? 'green': 'red'
    ctx.fillRect(snake[i].x, snake[i].y, box, box)


  }

  ctx.fillStyle = 'white'
  ctx.font = '50px Arial'
  ctx.fillText(score, box * 2.5, box * 1.7)

  let snakeX = snake[0].x
  let snakeY = snake[0].y

  if(snakeX === food.x && snakeY === food.y){
    food = {
      x: Math.floor(Math.random() * 17 + 1) * box,
      y: Math.floor(Math.random() * 15 + 3) * box,
    }
    score += 1;
  }else{
    snake.pop();
  }

  if(snakeX < box || snakeX > box * 17 ||
     snakeY < box * 3 || snakeY > box * 17){
       clearInterval(game)
     }

  if(dir === 'left'){
    snakeX += box
  }else if(dir === 'right'){
    snakeX -= box
  }else if(dir === 'top'){
    snakeY -= box
  }else if(dir === 'bottom'){
    snakeY += box
  }

  let newHead = {
    x: snakeX,
    y: snakeY
  }

  eatTail(newHead, snake)

  snake.unshift(newHead)

}

let game = setInterval(drawGame, 100)
